package ch.ffhs.ftoop.p1.divisor;

import java.util.concurrent.ExecutionException;

import student.TestCase;



/**
 * Hinweis: Die Unit Tests haben einen festen Timeout von 10 sekunden - achten
 * Sie daher darauf, dass Sie das Testintervall nicht zu gross gestalten.
 * 
 * Unittests, die die main-Methode mit unterschiedlichen Argumenten Aufrufen.
 * Es wird der systemOut gepr�ft (Ausgabe am Schluss).
 * 
 * @author Sandrine M�ller
 * @author Melanie Calame
 * 
 */

public class CalculateDivisorTest extends TestCase {
//Test1: Testet im Intervall von 10-10000 mit 4 Threads
	public void testCalculate() throws InterruptedException, ExecutionException {
		CalculateDivisor.main(new String[] { "10", "10000", "4" });
		assertFuzzyEquals(
				"Ergebnis: Zahl mit maximaler Anzahl Divisoren: 7560 (64 Divisoren)\n",
				systemOut().getHistory());
	}
//Test2: Testet im Intervall von 1-100 mit 10 Threads	
	public void testCalculate2() throws InterruptedException, ExecutionException {
		CalculateDivisor.main(new String[] { "1", "100", "10" });
		assertFuzzyEquals(
				"Ergebnis: Zahl mit maximaler Anzahl Divisoren: 60 (12 Divisoren)\n",
				systemOut().getHistory());
	}
//Test3: Testet im Intervall von 10-10000 mit 1000 Threads	
	public void testCalculate3() throws InterruptedException, ExecutionException {
		CalculateDivisor.main(new String[] { "10", "10000", "1000" });
		assertFuzzyEquals(
				"Ergebnis: Zahl mit maximaler Anzahl Divisoren: 7560 (64 Divisoren)\n",
				systemOut().getHistory());
	}

}
