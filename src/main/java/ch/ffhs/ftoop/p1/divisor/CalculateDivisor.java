package ch.ffhs.ftoop.p1.divisor;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Das folgende Programm soll aus einem vorgegebene Intervall von Long-Zahlen
 * die Zahl zur�ckgeben, die die meisten Divisoren hat. Sollte es mehrere solche
 * Zahlen geben, so soll die kleinste dieser Zahlen ausgegeben werden.
 * 
 * Die Berechnung soll in n Threads stattfinden, die via Executor Framework
 * gesteuert werden, und sich das Problem aufteilen - jeder Thread soll eine
 * Teilmenge des Problems l�sen. Verwenden Sie bitte einen FixedThreadPool und
 * implementieren Sie die Worker als Callable.
 * 
 * @author Sandrine M�ller
 * @author Melanie Calame
 * @version 2.0
 * 
 */
public class CalculateDivisor {

	private long von, bis, intervall;
	private int threadCount;
	private ExecutorService executorService;
	private ArrayList<Future<long[]>> futureList;

	/**
	 * @param von
	 *            untere Intervallgrenze
	 * @param bis
	 *            obere Intervallgrenze
	 * @param threadCount
	 *            Abzahl der Threads, auf die das Problem aufgeteilt werden soll
	 */
	public CalculateDivisor(long von, long bis, int threadCount) {
		this.von = von;
		this.bis = bis;
		this.threadCount = threadCount;
		/*
		 * In der variable intervall wird die Gr�sse der Intervalle f�r die
		 * einzelnen Threads gespeichert
		 */
		intervall = (this.bis - this.von) / this.threadCount;
		// Ein Thread-Pool mit fixer Gr�sse (threadCount) wird erstellt
		executorService = Executors.newFixedThreadPool(this.threadCount);
		futureList = new ArrayList<Future<long[]>>();

		/*
		 * In dieser for-Schleife werden die Threads erzeugt und gestartet. Sie
		 * werden als Future<long[]>-Objekte in der futureList gespeichert. Der
		 * letzte Thread wird ausserhalb der Schleife erzeugt und gestartet, da
		 * sonst der genaue Endwert des Intervalls nicht als End-Argument
		 * �bergeben wird.
		 */
		for (int i = 0; i < this.threadCount - 1; i++) {
			futureList.add(executorService.submit(new CalculatingThread(this.von, this.von + intervall)));
			this.von = this.von + intervall + 1;
		}
		futureList.add(executorService.submit(new CalculatingThread(this.von, this.bis)));

	}

	/**
	 * Berechnungsroutine: Die durch die Threads berechneten Werte werden durch
	 * das Future-Interface und deren Methode get() abgerufen und aus diesen
	 * Werten, die in der resultList zwischengespeichert werden, wird das beste
	 * Werte-Paar gesucht. (gr�sste #Divisoren, bei gleicher # gewinnt der
	 * kleinere Ziffernwert)
	 * 
	 * @return new DivisorResult(bestValue, bestDivisorCounter) Es wird ein
	 *         neues DivisorResult-Object mit den Argumenten bestValue und
	 *         bestDivisorCounter erstellt und zur�ckgegeben. Da es an einen
	 *         String zur�ckgegeben wird, wird die toString()-Methode des
	 *         DivisorResult-Objektes aufgerufen.
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	DivisorResult calculate() throws InterruptedException, ExecutionException {
		long bestValue = 0, bestDivisorCounter = 0;
		ArrayList<long[]> resultList = new ArrayList<long[]>();
		try {
			for (int i = 0; i < this.threadCount; i++) {
				resultList.add(futureList.get(i).get());
			}
			bestValue = resultList.get(0)[0];
			bestDivisorCounter = resultList.get(0)[1];
			for (int e = 1; e < threadCount; e++) {

				if (resultList.get(e)[1] > bestDivisorCounter) {
					bestDivisorCounter = resultList.get(e)[1];
					bestValue = resultList.get(e)[0];
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// ExecutorService wird beendet. Alle Threads werden beendet.
		executorService.shutdown();
		return new DivisorResult(bestValue, bestDivisorCounter);
	}
	
	/**
	 * Methode, die überprüft, dass valide Argumente über die Konsole angegeben werden. 
	 * @param Konsolenargumente werden übergeben
	 */
	public static void usageControl(String[] args) {
		//Stellt sicher, dass das Programm richtig angewendet wird, die richtige Anzahl Argumente mitgegeben wird
		if (args.length != 3) {
			System.out
					.println("Usage: CalculateDivisor <intervalStart> <intervalEnd> <threadCount>");
			System.exit(1);
		}
		//Stellt sicher, dass das Intervall nur wie vorgesehen eingegeben wird
		if (Long.parseLong(args[0]) > Long.parseLong(args[1])) {
			System.out.println("Usage: The interval start must be lower than the interval end");
			System.exit(1);
		}
		//Stellt sicher, dass eine valide Anzahl threads mitgegeben wird
		if(Long.parseLong(args[2]) < 1) {
			System.out.println("Usage: The number of threads in the thread pool must be at least one");
			System.exit(1);
		}
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		usageControl(args);
		long von = Long.parseLong(args[0]);
		long bis = Long.parseLong(args[1]);
		int threads = Integer.parseInt(args[2]);

		CalculateDivisor cp = new CalculateDivisor(von, bis, threads);
		System.out.println("Ergebnis: " + cp.calculate());
	}

}

/**
 * H�lt das Ergebnis einer Berechnung
 * 
 * @author Peter Tellenbach
 * 
 * 
 */
class DivisorResult {
	// das eigentlich ergebnis - die Zahl mit der max. Anzahl von Divisoren
	long result;

	// Anzahl der Divisoren von Result
	long countDiv;

	public DivisorResult(long r, long c) {
		result = r;
		countDiv = c;
	}

	public long getResult() {
		return result;
	}

	public long getCountDiv() {
		return countDiv;
	}
	//Wird verwendet, um am Schluss das Ergebnis in einem "sch�nen" String ausgeben zu k�nnen.
	@Override
	public String toString() {
		return "Zahl mit maximaler Anzahl Divisoren: " + result + " (" + countDiv + " Divisoren)";
	}

}
