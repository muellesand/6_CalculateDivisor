package ch.ffhs.ftoop.p1.divisor;

import java.util.concurrent.Callable;

/**
 * Die Klasse CalculatingThread implementiert das Interface Callable. Instanzen
 * sind also Threads, die eine R�ckgabe liefern k�nnen. (hier: long[]) Der
 * Thread berechnet in einem vorgegebenen Intervall (start-end) die Zahl, mit
 * den meisten Divisoren und gibt diese Zahl(bestValue) und die dazugeh�rige
 * Anzahl Divisoren (bestDivisorCounter) als R�ckgabewert der Methode call()
 * zur�ck. Der Main-Thread kann durch Instanzen des Interfaces Future<long[]>
 * diese R�ckgabewerte abfragen.
 * 
 * 
 * @author Sandrine M�ller
 * @author Melanie Calame
 * @version 2.0
 *
 *
 */
public class CalculatingThread implements Callable<long[]> {
	private long start, end;

	/**
	 * @param start
	 *            Anfangswert des zu untersuchenden Intervalls
	 * @param end
	 *            Endwert des zu untersuchenden Intervalls
	 */
	CalculatingThread(long start, long end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public long[] call() throws Exception {
		long[] results = new long[2];
		long divisorCounter;
		long bestValue = 0;
		long bestDivisorCounter = 0;

		/*
		 * Jede Zahl ist durch sich selber teilbar. Darum ist der Counter schon
		 * auf 1 gesetzt. (Schleife mit Variable d geht nur bis value/2 und
		 * kommt somit nie auf die zu untersuchende Zahl selber.)
		 */
		for (long value = start; value <= end; value++) {
			divisorCounter = 1;//
			/*
			 * Schleife untersucht Divisoren von 1 bis Zahl/2, da danach nur die
			 * Zahl selber ein Divisor ist.
			 */
			for (long d = 1; d <= value / 2; d++) {
				if (value % d == 0) {
					// Counter wird bei positivem Divisionstest erh�ht.
					divisorCounter++;
				}
			}
			/*
			 * Vergleicht die # Divisoren der aktuellen Zahl mit der h�chsten
			 * bisher berechneten # Divisoren Falls die gerade berechnete Anzahl
			 * h�her ist, wird dieser Wert und die dazugeh�rige Zahl in den
			 * Variablen bestDivisorCounter und bestValue gespeichert.
			 */
			if (divisorCounter > bestDivisorCounter) {
				bestDivisorCounter = divisorCounter;
				bestValue = value;
			}
		}
		// Schreibt den "besten" Wert in das long-Array.
		results[0] = bestValue;
		// Schreibt die dazugeh�rige #Divisoren an die zweite Stelle des Arrays.
		results[1] = bestDivisorCounter;
		// gibt das Array zur�ck
		return results;
	}

}
